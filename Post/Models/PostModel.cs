﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Post.Models
{
    public class PostModel
    {
        private AppDbContext _db;
        public PostModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<Posts> GetAll()
        {
            return _db.Posts.ToList<Posts>();
        }

        public string GetByCaption(int id)
        {
            Posts post = _db.Posts.First(p => p.PostId == id);
            return post.Caption;
        }
    }
}
