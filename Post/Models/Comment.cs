﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Post.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }

        [Required]
        public int UserId { get; set; }
                                         
        [Required]
        public int PostId { get; set; }

        [Required]
        [StringLength(2000)]
        public string ActualComment { get; set; }
    }
}
