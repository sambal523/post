﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Post.Models
{
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PostId { get; set; }
        
        [Required]
        public int UserId { get; set; }

        [Required]
        public int TagId { get; set; }

        [Required]
        public int ImageId { get; set; }

        [Required]
        [StringLength(200)]
        public string Caption { get; set; }  

        public int AmountOfLikes { get; set; }
        public int AmountOfDislikes { get; set; }
    }
}
