﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Post.Models
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Posts>().ToTable("Posts")
                .Property(p => p.PostId).UseSqlServerIdentityColumn();
            builder.Entity<Comment>().ToTable("Comments")
                .Property(c => c.CommentId).UseSqlServerIdentityColumn();
            builder.Entity<Image>().ToTable("Images")
                .Property(i => i.ImageId).UseSqlServerIdentityColumn();
            builder.Entity<Tag>().ToTable("Tags")
                .Property(t => t.TagId).UseSqlServerIdentityColumn();
        }
                         
        public virtual DbSet<Posts> Posts { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
    }
}
