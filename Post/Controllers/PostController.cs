﻿using Microsoft.AspNetCore.Mvc;
using Post.Models;
using Post.ViewModels;

namespace Post.Controllers
{
    public class PostController : Controller
    {
        AppDbContext _db;
        public PostController(AppDbContext context)
        {
            _db = context;
        }
        public IActionResult Index()
        {
            PostModel model = new PostModel(_db);
            PostViewModel viewModel = new PostViewModel();
            viewModel.Posts = model.GetAll();
            return View(viewModel);
        }
    }
}