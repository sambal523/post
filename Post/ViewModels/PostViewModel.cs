﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using Post.Models;

namespace Post.ViewModels
{
    public class PostViewModel
    {
        public PostViewModel() { }
        public string Caption { get; set; }
        public List<Posts> Posts { get; set; }
        public IEnumerable<SelectListItem> GetByTags()
        {
            //return Posts.Select(post => new SelectListItem { Text = post.TagId.ToString(), Value = post.tagName});
            return Posts.Select(post => new SelectListItem { Text = post.TagId.ToString(), Value = post.TagId.ToString() });
        }
    }
}